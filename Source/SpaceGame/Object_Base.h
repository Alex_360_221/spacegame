// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Object_Base.generated.h"

UCLASS()
class SPACEGAME_API AObject_Base : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AObject_Base(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh")
		UStaticMeshComponent* MeshComp;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Physics Stuff")
		FVector velocity;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Physics Stuff")
		FVector angularVelocity;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Physics Stuff")
		float mass;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Physics Stuff")
		FVector centerOfMass;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Physics Stuff")
		float gravityForce;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Physics Stuff")
		FVector gravity;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Physics Stuff")
		FVector force;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Update(float DeltaTime);
};
