// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "Components/CapsuleComponent.h"
#include <SpaceGame/Planets/PlanetController.h>
//#include <SpaceGame/Planets/PlanetMesh.h>

// Sets default values
APlayerCharacter::APlayerCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->InitCapsuleSize(20.f, 40.0f);

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	CameraComp->SetupAttachment(GetCapsuleComponent());
	CameraComp->bUsePawnControlRotation = true;
	CameraComp->SetRelativeLocation(FVector(0, 0, 15));
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	mapRef = GetWorld()->SpawnActor<APlanetController>(FVector(1000,0,1000), FRotator::ZeroRotator);
	if (mapRef != nullptr)
	{
		GLog->Log("player");
		mapRef->playerRef = this;
		mapRef->Start();
		if(mapRef->playerRef != nullptr){ GLog->Log("yyyyyyyyy"); }
	}	
}

void APlayerCharacter::MoveForward(float vaule)
{
	if ((Controller) && (vaule != 0.0f))
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), vaule);
	}
}

void APlayerCharacter::MoveRight(float vaule)
{
	if ((Controller) && (vaule != 0.0f))
	{
		AddMovementInput(GetActorRightVector(), vaule);
	}
}

void APlayerCharacter::TurnAtRate(float vaule)
{
	APlayerCharacter::AddControllerYawInput(vaule);
}

void APlayerCharacter::LookUpRate(float vaule)
{
	APlayerCharacter::AddControllerPitchInput(vaule);
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);//uses the inbuilt jump function
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	InputComponent->BindAxis("MoveX", this, &APlayerCharacter::MoveForward);
	InputComponent->BindAxis("MoveY", this, &APlayerCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APlayerCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APlayerCharacter::LookUpRate);
}

