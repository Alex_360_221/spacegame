// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Materials/MaterialInstance.h"
#include "PlanetMesh.generated.h"

UCLASS()
class SPACEGAME_API APlanetMesh : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlanetMesh(const FObjectInitializer& ObjectInitializer);

	UProceduralMeshComponent* terrainMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterialInstance* terrainMat;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int extraVertices;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<FVector> vertices;
	TArray<int> triangles;
	TArray<FVector2D> UVs;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<FVector> vertices_Full;
	TArray<int> triangles_Full;
	TArray<FVector2D> UVs_Full;
	TArray<int> normalsIndices;	//this stores which normal should be used for the mesh

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	class APlayerCharacter* playerRef;
	class APlanetController* mapRef;

	float vertexDistance;
	int vertexCount;
	int chunk_LOD;
	//class APlayerCharacter* playerRef;
	//class AMapController* mapRef;
	APlanetMesh* parent;
	void Start();	//this will start the mesh creation after the varibles have been set

private:
	bool SetVaules;
	int bottomTriPoint, topTriPoint;

	//tr == x+ y+
	//bl == x- y-
	bool hasDivided;
	APlanetMesh* tL;
	APlanetMesh* tR;
	APlanetMesh* bL;
	APlanetMesh* bR;

	void CreateMesh();
	void AddAllTriangles();	//this wil create all tringles
	void AddTriangle();	//this will add a square (two triangles
	void UpdateLOD();	//this will workout how far away th
public:
	void RemoveLOD(); //this will remove the lod for this model and done
};
