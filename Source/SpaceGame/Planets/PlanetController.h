// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlanetController.generated.h"

UCLASS()
class SPACEGAME_API APlanetController : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlanetController();
	class APlayerCharacter* playerRef;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Start();

private:
	//render settings
	float vertexDistance;
	int vertexCount;
	float chunkSize;

	TMap<FVector, class APlanetMesh*> chunks;	//array of chunks

	void SpawnChunks(FVector playerPos);	//this will loop through all the chunk poses checking if a chunk should be there
	void SpawnChunk(FVector chunkPos, FRotator rotation);	//this will spawn one chunk

public:
	class APlanetMesh* SpawnChunkAtPosWithLOD(FVector chunkPos, int LOD, class APlanetMesh* parent, float newVertexDistance);	//this will spawn a chunk at a certain pos with a certian level of detail

};
