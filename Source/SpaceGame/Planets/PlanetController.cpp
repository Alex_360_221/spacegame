// Fill out your copyright notice in the Description page of Project Settings.


#include "PlanetController.h"
#include "PlanetMesh.h"

// Sets default values
APlanetController::APlanetController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	vertexDistance = 400;
	vertexCount = 16;
	chunkSize = vertexDistance * 16;
	playerRef = nullptr;
}

// Called when the game starts or when spawned
void APlanetController::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlanetController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APlanetController::Start()
{
	SpawnChunks(FVector(0, 0, 0));
}

void APlanetController::SpawnChunks(FVector playerPos)
{
	FVector actorPos = FVector(-5000.0,0,2000);// = GetActorLocation();
	SpawnChunk(actorPos, FRotator(0,0,0/*-90.000000,0.000000,0.000000)*/));
}

void APlanetController::SpawnChunk(FVector chunkPos, FRotator rotation)
{
	//FVector newChunkPos = FVector(chunkPos.X, chunkPos.Y, 0);	
	APlanetMesh* chunk = GetWorld()->SpawnActor<APlanetMesh>(chunkPos, rotation);
	chunks.Add(chunkPos, chunk);
	if (chunk != nullptr)
	{
		chunk->mapRef = this;
		chunk->vertexCount = vertexCount;
		chunk->vertexDistance = vertexDistance;
		chunk->playerRef = this->playerRef;
		chunk->Start();
	}
}

APlanetMesh* APlanetController::SpawnChunkAtPosWithLOD(FVector chunkPos, int LOD, APlanetMesh* parent, float newVertexDistance)
{
	FVector newChunkPos = FVector(chunkPos.X, chunkPos.Y, 0);
	FRotator rot = parent->GetActorRotation();
	APlanetMesh* chunk = GetWorld()->SpawnActor<APlanetMesh>(newChunkPos, rot);
	if (chunk != nullptr)
	{
		chunk->parent = parent;
		chunk->chunk_LOD = LOD;
		chunk->mapRef = this;
		chunk->vertexCount = vertexCount;
		chunk->vertexDistance = newVertexDistance;
		chunk->playerRef = this->playerRef;
		chunk->Start();
		return chunk;
	}
	return nullptr;
}

