// Fill out your copyright notice in the Description page of Project Settings.

#include "PlanetMesh.h"
#include "KismetProceduralMeshLibrary.h"
#include "PlanetController.h"
#include "PlanetMesh.h"
#include <SpaceGame/Player/PlayerCharacter.h>
#include <SpaceGame/Lib/FastNoise.h>


// Sets default values
APlanetMesh::APlanetMesh(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	vertexDistance = 50;
	vertexCount = 16;
	extraVertices = 2;

	bottomTriPoint = 0;
	topTriPoint = 0;
	chunk_LOD = 0;
	SetVaules = false;
	hasDivided = false;

	terrainMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("Terrain Mesh"));
	terrainMesh->SetupAttachment(RootComponent);
	SetRootComponent(terrainMesh);

	terrainMat = CreateDefaultSubobject<UMaterialInstance>(TEXT("Terrain Mat"));
	//static ConstructorHelpers::FObjectFinder<UMaterialInterface>Material1(TEXT("Material'/Game/Mats/TerrainMat.TerrainMat'"));
	static ConstructorHelpers::FObjectFinder<UMaterialInstance>Material1(TEXT("MaterialInstanceConstant'/Game/Mats/TerrainMat_Inst.TerrainMat_Inst'"));
	terrainMat = Material1.Object;
	
	//inst = GetWorld()->GetParameterCollectionInstance(MaterialParameterCollectionAsset);
	//inst->SetVectorParameterValue(vector);
	
}

// Called when the game starts or when spawned
void APlanetMesh::BeginPlay()
{
	Super::BeginPlay();

	
}

// Called every frame
void APlanetMesh::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (SetVaules)
	{
		UpdateLOD();
	}
}

void APlanetMesh::Start()
{
	SetVaules = true;
	CreateMesh();
}

void APlanetMesh::CreateMesh()
{
	GLog->Log("iodhiodhodi");
	//loop through vertices on x and y
	//create vertex data 
	FVector chunkPos = GetActorLocation();
	float uvScale = 1 / (vertexDistance * (vertexCount + (extraVertices * 2)));
	FastNoise fn;
	fn.SetNoiseType(FastNoise::Perlin);
	fn.SetSeed(74874);
	FVector vertexPos = FVector(0, 0, 0);
	int halfVertexCount = vertexCount / 2;
	for (int x = (halfVertexCount + extraVertices) * -1; x <= halfVertexCount/*vertexCount*/ + extraVertices; x++)
	{
		vertexPos.X = x * vertexDistance;
		for (int y = (halfVertexCount + extraVertices) * -1; y <= halfVertexCount/*vertexCount*/ + extraVertices; y++)
		{
			vertexPos.Y = y * vertexDistance;
			//get Noise

			vertexPos.Z = (fn.GetNoise(((vertexPos.X + chunkPos.X) * 0.1), ((vertexPos.Y + chunkPos.Y) * 0.1)) * 250);
			if (x >= halfVertexCount * -1 && x <= halfVertexCount && y >= halfVertexCount * -1 && y <= halfVertexCount)
			{//in section which needs to be part of the mesh
				vertices.Add(vertexPos);
				int index = vertices_Full.Add(vertexPos);
				normalsIndices.Add(index);
				UVs.Add(FVector2D(vertexPos.X * uvScale, vertexPos.Y * uvScale));
				UVs_Full.Add(FVector2D(vertexPos.X * uvScale, vertexPos.Y * uvScale));
			}
			else
			{
				vertices_Full.Add(vertexPos);
				UVs_Full.Add(FVector2D(vertexPos.X * uvScale, vertexPos.Y * uvScale));
			}
		}
	}

	////create mesh
	AddAllTriangles();

	TArray<FVector> normals_Full;
	TArray <FColor> VertexColour;
	TArray<FProcMeshTangent> Tangents_Full;
	UKismetProceduralMeshLibrary::CalculateTangentsForMesh(vertices_Full, triangles_Full, UVs_Full, normals_Full, Tangents_Full);
	//chose whcih normals to use
	TArray<FVector> normals;
	TArray<FProcMeshTangent> Tangents;
	for (int i = 0; i < normalsIndices.Num();i++)
	{
		normals.Add(normals_Full[normalsIndices[i]]);
		Tangents.Add(Tangents_Full[normalsIndices[i]]);
	}

	terrainMesh->CreateMeshSection(0, vertices, triangles, normals, UVs, VertexColour, Tangents, true);
	//terrainMesh->CreateMeshSection(0, vertices_Full, triangles_Full, normals_Full, UVs_Full,VertexColour, Tangents_Full,true);
	terrainMesh->SetMaterial(0, terrainMat);
}

void APlanetMesh::AddAllTriangles()
{
	for (int32 x = 0; x < (vertexCount/* -1*/); x++)//run trhough all the x vertices 
	{
		for (int32 y = 0; y < (vertexCount/*-1*/); y++)//run trhough all the y vertices 
		{
			AddTriangle();//adds tringles at that point
		}
		bottomTriPoint = bottomTriPoint + 1;//puts the bottom point onto the next line
		topTriPoint = topTriPoint + 1;//puts the top point onto the next line
	}

	bottomTriPoint = 0;
	topTriPoint = 0;
	for (int32 x = 0; x < (vertexCount + (extraVertices * 2)/* -1*/); x++)//run trhough all the x vertices 
	{
		for (int32 y = 0; y < (vertexCount + (extraVertices * 2)/*-1*/); y++)//run trhough all the y vertices 
		{
			//AddTriangle();//adds tringles at that point
			triangles_Full.Add(bottomTriPoint);//adds the bottom point to the tringles array
			bottomTriPoint = bottomTriPoint + 1;
			triangles_Full.Add(bottomTriPoint);//adds the bottom point to the tringles array
			topTriPoint = bottomTriPoint + vertexCount + (extraVertices * 2)/* - 1*/;
			triangles_Full.Add(topTriPoint);//adds the bottom point to the tringles array
			//one tringle complete
			triangles_Full.Add(topTriPoint);//adds the top point to the tringles array

			triangles_Full.Add(bottomTriPoint);//adds the bottom point to the tringles array
			//bottomTriPoint = bottomTriPoint + 1;
			topTriPoint = topTriPoint + 1;
			triangles_Full.Add(topTriPoint);//adds the top point to the tringles array
			//second tringle done
		}
		bottomTriPoint = bottomTriPoint + 1;//puts the bottom point onto the next line
		topTriPoint = topTriPoint + 1;//puts the top point onto the next line
	}
}

void APlanetMesh::AddTriangle()
{
	triangles.Add(bottomTriPoint);//adds the bottom point to the tringles array
	bottomTriPoint = bottomTriPoint + 1;
	triangles.Add(bottomTriPoint);//adds the bottom point to the tringles array
	topTriPoint = bottomTriPoint + vertexCount/* - 1*/;
	triangles.Add(topTriPoint);//adds the bottom point to the tringles array
	//one tringle complete
	triangles.Add(topTriPoint);//adds the top point to the tringles array

	triangles.Add(bottomTriPoint);//adds the bottom point to the tringles array
	//bottomTriPoint = bottomTriPoint + 1;
	topTriPoint = topTriPoint + 1;
	triangles.Add(topTriPoint);//adds the top point to the tringles array
	//second tringle done
}

void APlanetMesh::UpdateLOD()
{
	if (parent == nullptr && hasDivided == false)
	{
		// chunk at highest level
		//should this chunk subdivide?
		//GLog->Log("fiuhfiubfi!!");
		if (playerRef == nullptr) { return; }
		float dis = FVector::Distance(playerRef->GetActorLocation(), GetActorLocation());
		if (dis < 10000)
		{
			hasDivided = true;
			float halfChunkSize = ((vertexDistance / 2) * vertexCount) / 2;
			FVector center = GetActorLocation();
			float newVertexDistance = vertexDistance / 2;
			//spawn new chunks here
			tR = mapRef->SpawnChunkAtPosWithLOD(FVector(center.X + halfChunkSize, center.Y + halfChunkSize, center.Z), chunk_LOD + 1, this, newVertexDistance);
			tL = mapRef->SpawnChunkAtPosWithLOD(FVector(center.X + halfChunkSize, center.Y - halfChunkSize, center.Z), chunk_LOD + 1, this, newVertexDistance);
			bL = mapRef->SpawnChunkAtPosWithLOD(FVector(center.X - halfChunkSize, center.Y - halfChunkSize, center.Z), chunk_LOD + 1, this, newVertexDistance);
			bR = mapRef->SpawnChunkAtPosWithLOD(FVector(center.X - halfChunkSize, center.Y + halfChunkSize, center.Z), chunk_LOD + 1, this, newVertexDistance);
			//remove this mesh
			terrainMesh->SetVisibility(false);
		}
	}
	else if (parent != nullptr && hasDivided == false)
	{		
		//has parent and not divided
		if (playerRef == nullptr) { return; }
		float dis = FVector::Distance(playerRef->GetActorLocation(), GetActorLocation());
		if (dis < 10000 / (chunk_LOD * 2))
		{
			if (chunk_LOD >= 3) { return; }
			hasDivided = true;
			float halfChunkSize = ((vertexDistance / 2) * vertexCount) / 2;
			FVector center = GetActorLocation();
			float newVertexDistance = vertexDistance / 2;
			//spawn new chunks here
			tR = mapRef->SpawnChunkAtPosWithLOD(FVector(center.X + halfChunkSize, center.Y + halfChunkSize, center.Z), chunk_LOD + 1, this, newVertexDistance);
			tL = mapRef->SpawnChunkAtPosWithLOD(FVector(center.X + halfChunkSize, center.Y - halfChunkSize, center.Z), chunk_LOD + 1, this, newVertexDistance);
			bL = mapRef->SpawnChunkAtPosWithLOD(FVector(center.X - halfChunkSize, center.Y - halfChunkSize, center.Z), chunk_LOD + 1, this, newVertexDistance);
			bR = mapRef->SpawnChunkAtPosWithLOD(FVector(center.X - halfChunkSize, center.Y + halfChunkSize, center.Z), chunk_LOD + 1, this, newVertexDistance);
			//remove this mesh
			terrainMesh->SetVisibility(false);
			GLog->Log("Adding more detial!!");
		}		
	}
	else if (parent != nullptr && hasDivided == true)
	{
		float dis = FVector::Distance(playerRef->GetActorLocation(), GetActorLocation());
		if (dis > (10000 / ((chunk_LOD) * 2)))
		{			
			GLog->Log("Try to remove detial");
			RemoveLOD();

			terrainMesh->SetVisibility(true);
			GLog->Log("Removing Detail");
			hasDivided = false;
		}
	}	
}

void APlanetMesh::RemoveLOD()
{
	if (hasDivided)
	{
		tR->RemoveLOD();
		tL->RemoveLOD();
		bR->RemoveLOD();
		bL->RemoveLOD();
		tR->Destroy();
		tL->Destroy();
		bR->Destroy();
		bL->Destroy();
	}	
}

