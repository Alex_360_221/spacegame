// Fill out your copyright notice in the Description page of Project Settings.


#include "Object_Base.h"
#include "Components/PrimitiveComponent.h"

// Sets default values
AObject_Base::AObject_Base(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	MeshComp = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, (TEXT("MeshComp")));
	static ConstructorHelpers::FObjectFinder<UStaticMesh>Mesh01(TEXT("StaticMesh'/Game/Models/Engines/Rocket.Rocket'"));
	UStaticMesh* assset = Mesh01.Object;
	MeshComp->SetStaticMesh(assset);
	//MeshComp->SetMaterial(0, mat);
	SetRootComponent(MeshComp);
	velocity = FVector(0, 0, 0);
	angularVelocity = FVector(0, 0, 0);

	mass = 10.0f;
	centerOfMass = FVector(0, 0, 0);
	gravityForce = -1;
	gravity = FVector(0, 0, 1);
	
	MeshComp->SetSimulatePhysics(true);
	MeshComp->SetEnableGravity(false);


	
}

// Called when the game starts or when spawned
void AObject_Base::BeginPlay()
{
	Super::BeginPlay();
//	MeshComp->OnComponentHit.AddDynamic(this, &AObject_Base::OnCollision);
//	MeshComp->OnComponentBeginOverlap.AddDynamic(this, &AObject_Base::OnOverLap);

	
}

// Called every frame
void AObject_Base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//MeshComp->SetAllPhysicsLinearVelocity(velocity);
	//MeshComp->SetCenterOfMass(centerOfMass);
	//MeshComp->AddForceAtLocation(FVector(0,0,1),FVector(0,0,0));
	velocity = MeshComp->GetPhysicsLinearVelocity() + (force / mass) + (gravity * gravityForce);
	//angularVelocity = MeshComp->GetPhysicsAngularVelocity() + angularVelocity;
	//SetActorLocation(GetActorLocation() + (velocity * DeltaTime));
	//MeshComp->getCol
	//MeshComp->collision
	//FVector ue4Velocity = MeshComp->GetComponentVelocity();
	MeshComp->SetPhysicsLinearVelocity(velocity);
	MeshComp->SetPhysicsAngularVelocity(angularVelocity);
	//MeshComp->AddForceAtLocation(force,FVector(0,0,0));
	//if (ue4Velocity.X == 0 && ue4Velocity.Y == 0 && ue4Velocity.Z == 0) { velocity = FVector(0, 0, 0); }
}

void AObject_Base::Update(float DeltaTime)
{

}



